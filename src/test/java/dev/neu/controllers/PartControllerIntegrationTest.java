package dev.neu.controllers;

import dev.neu.BikePartShopApp;
import dev.neu.models.Part;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PartControllerIntegrationTest {

    private static BikePartShopApp app = new BikePartShopApp();

    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }

    @Test
    public void testGetAllPartsAuthorized() {
        HttpResponse<List<Part>> response = Unirest.get("http://localhost:7000/parts")
                .header("Authorization", "token")
                .asObject(new GenericType<List<Part>>() {});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

    /*
    @Test
    public void testAddPartAuthorized() {
        HttpResponse<List<Part>> response = Unirest.get("http://localhost:7000/parts")
                .header("Authorization", "token")
                .asObject(new GenericType<List<Part>>() {});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }
    */
}
