package dev.neu.controllers;

import dev.neu.models.Invoice;
import dev.neu.service.InvoiceService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for Invoice
 * Implements handle method of Handler interface
 * get and post operations are performed
 */

public class InvoiceController {
    private Logger logger = LoggerFactory.getLogger(InvoiceController.class);
    private InvoiceService invoiceService = new InvoiceService();

    public void handleAddInvoice(Context ctx) {
        Invoice invoice = ctx.bodyAsClass(Invoice.class);
        logger.info("Adding invoice number: {}", invoice.getInvoiceNumber());
        invoiceService.addInvoice(invoice);
        ctx.status(201);
    }

    public void handleGetInvoices(Context ctx) {
        String name = ctx.queryParam("name");
        if (name != null) {
            logger.info("Getting invoices based on an associate");
            ctx.json(invoiceService.getInvoicesByAssociate(name));
        } else {
            logger.info("Getting all invoices");
            ctx.json(invoiceService.getAllInvoices());
        }
    }
}
