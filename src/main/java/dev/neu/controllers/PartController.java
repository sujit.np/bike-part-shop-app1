package dev.neu.controllers;

import dev.neu.models.Part;
import dev.neu.service.PartService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for Part
 * Implements handle method of Handler interface
 * get, delete, put, post operations are performed
 */

public class PartController {

    private Logger logger = LoggerFactory.getLogger(PartController.class);
    private PartService partService = new PartService();

    public void handleAddPart(Context ctx) {
        Part part = ctx.bodyAsClass(Part.class);
        logger.info("Adding new part with part number {}", part.getPartNumber());
        partService.addPart(part);
        ctx.status(201); // 201 is for created
    }

    public void handleGetAllParts(Context ctx) {
        logger.info("Getting all parts");
        ctx.json(partService.getAllParts());
    }

    public void handleGetPartByPartNumber(Context ctx) {
        String partNumber = ctx.pathParam("partNumber");
        if (partNumber.matches("\\d+$")) {
            int partNum = Integer.parseInt(partNumber);
            Part part = partService.getPartByPartNumber(partNum);
            if (part == null) {
                logger.warn("Part number {} is not found", partNum);
                throw new NotFoundResponse("Part number \"" + partNum + "\" is not found");
            } else {
                logger.info("Getting part with part number {}", partNum);
                ctx.json(part);
            }
        } else {
            throw new BadRequestResponse("input \""+partNumber + "\"cannot be parsed");
        }
    }

    public void handleDeletePartByName(Context ctx) {
        String partName = ctx.queryParam("partName");
        logger.info("Deleting {}", partName);
        partService.deletePartByName(partName);
    }

    public void handleUpdateRetailPrice(Context ctx) {
        String partNumber = ctx.pathParam("partNumber");
        int partNum = 0;
        if (partNumber.matches("\\d+$")) {
            partNum = Integer.parseInt(partNumber);
        } else {
            throw new BadRequestResponse("Input part number " + partNumber + " cannot be parsed");
        }
        String newPrice = ctx.queryParam("newPrice");
        double partPrice = 0;
        if (newPrice.matches("\\d+.\\d{1,2}")) {
            partPrice = Double.parseDouble(newPrice);
        } else {
            throw new BadRequestResponse("Enter part's price correctly(e.g. 10.00)");
        }
        logger.info("Updating {}'s price", partNumber);
        partService.updateRetailPrice(partNum, partPrice);
    }
}
