package dev.neu.service;

import dev.neu.data.PartDao;
import dev.neu.models.Part;

import java.util.List;

public class PartService {

    private PartDao partDao = new PartDao();

    public PartService() {
        super();
    }

    public Part addPart(Part part) {
        return partDao.addPart(part);
    }

    public List<Part> getAllParts() {
        return partDao.getAllParts();
    }

    public Part getPartByPartNumber(int partNum) {
        return partDao.getPartByPartNumber(partNum);
    }

    public void deletePartByName(String partName) {
        partDao.deletePartByName(partName);
    }

    public void updateRetailPrice(int partNumber, double newPrice) {
        partDao.updateRetailPrice(partNumber, newPrice);
    }
}
