package dev.neu.models;

import java.util.Objects;

public class Part {
    private int partNumber;
    private String partName;
    private double retailPrice;

    public Part() {
        super();
    }

    public Part(int partNumber, String partName, double retailPrice) {
        super();
        this.partNumber = partNumber;
        this.partName = partName;
        this.retailPrice = retailPrice;
    }

    public int getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(int partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Part part = (Part) o;
        return partNumber == part.partNumber && Double.compare(part.retailPrice, retailPrice) == 0 && partName.equals(part.partName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partNumber, partName, retailPrice);
    }

    @Override
    public String toString() {
        return "Part{" +
                "partNumber=" + partNumber +
                ", partName='" + partName + '\'' +
                ", retailPrice=" + retailPrice +
                '}';
    }
}
