package dev.neu.models;

public class Employee extends Associate {

    public Employee() {
        super();
    }

    public Employee(String fullName, String phoneNumber, String emailAddress, Boolean employee) {
        super(fullName, phoneNumber, emailAddress, employee);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", employee=" + employee +
                '}';
    }
}
