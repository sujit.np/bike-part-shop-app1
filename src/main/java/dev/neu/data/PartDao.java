package dev.neu.data;

import dev.neu.models.Part;
import dev.neu.util.ConnectionUtil;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to manipulate part table in database
 * add, delete, update and query operations are performed on the part table
 */

public class PartDao {
    private Logger logger = LoggerFactory.getLogger(PartDao.class);

    public PartDao() {
        super();
    }

    public Part addPart(Part part) {
        // Check if part already exists
        // as our part_number has unique constraint in db
        if (getPartByPartNumber(part.getPartNumber()) != null) {
            logger.info("Part already exists");
            throw new BadRequestResponse("Part already exists");
        }
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into part (part_number, part_name, retail_price) values (?, ?, ?)");
            preparedStatement.setInt(1, part.getPartNumber());
            preparedStatement.setString(2, part.getPartName());
            preparedStatement.setDouble(3, part.getRetailPrice());
            preparedStatement.executeUpdate();
            logger.info("Part added successfully to db");
        } catch (SQLException e) {
            logException(e);
        }
        return part;
    }

    public List<Part> getAllParts() {
        List<Part> allParts = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from part");
            while (resultSet.next()) {
                int partNumber = resultSet.getInt("part_number");
                String partName = resultSet.getString("part_name");
                double retailPrice = resultSet.getDouble("retail_price");
                Part part = new Part(partNumber, partName, retailPrice);
                allParts.add(part);
            }
            logger.info("Selecting all parts from DB, retrieved {} parts", allParts.size());
        } catch (SQLException e) {
            logException(e);
        }
        return allParts;
    }

    public Part getPartByPartNumber(int partNumber) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from part where part_number = ?");
            preparedStatement.setInt(1, partNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String partName = resultSet.getString("part_name");
                double retailPrice = resultSet.getDouble("retail_price");
                return new Part(partNumber, partName, retailPrice);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    public int getPartIdByPartName(String partName) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from part where part_name = ?");
            preparedStatement.setString(1, partName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            logException(e);
        }
        return 0;
    }

    public void deletePartByName(String partName) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from part where part_name = ?");
            preparedStatement.setString(1, partName);
            preparedStatement.executeUpdate();
            logger.info("Deleted part: {}", partName);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void updateRetailPrice(int partNumber, double newPrice) {
        if (getPartByPartNumber(partNumber) == null) {
            logger.info("No part with number {}", partNumber);
            throw new NotFoundResponse("Part with number: " + partNumber + " is not found");
        } else {
            try(Connection connection = ConnectionUtil.getConnection()) {
                PreparedStatement preparedStatement = connection.prepareStatement("update part set retail_price = ? where part_number = ?");
                preparedStatement.setDouble(1, newPrice);
                preparedStatement.setInt(2, partNumber);
                preparedStatement.executeUpdate();
                logger.info("Updated part {}'s price to {}", partNumber, newPrice);
            } catch (SQLException e) {
                logException(e);
            }
        }
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }
}
