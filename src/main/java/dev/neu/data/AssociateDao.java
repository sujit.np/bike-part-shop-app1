package dev.neu.data;

import dev.neu.models.Associate;
import dev.neu.models.Customer;
import dev.neu.models.Employee;
import dev.neu.util.ConnectionUtil;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Communicates and update Employee and Customer table in database
 */

public class AssociateDao {

    private Logger logger = LoggerFactory.getLogger(AssociateDao.class);

    public Associate addAssociate(Associate associate) {
        PreparedStatement preparedStatement;
        try (Connection connection = ConnectionUtil.getConnection()) {
            if (associate.isEmployee()) {
                // add to employee table
                preparedStatement = connection.prepareStatement("insert into employee " +
                        "(full_name, phone_number, email_address) values (?, ?, ?)");
            } else {
                // add to customer table
                preparedStatement = connection.prepareStatement("insert into customer " +
                        "(full_name, phone_number, email_address) values (?, ?, ?)");
            }
            preparedStatement.setString(1, associate.getFullName());
            preparedStatement.setString(2, associate.getPhoneNumber());
            preparedStatement.setString(3, associate.getEmailAddress());
            preparedStatement.executeUpdate();
            if (associate.isEmployee()) {
                logger.info("Employee added successfully to db");
            } else {
                logger.info("Customer added successfully to db");
            }
        } catch (SQLException e) {
            logException(e);
        }
        return associate;
    }

    public List<Customer> getAllCustomers() {
        List<Customer> allCustomers = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from customer");
            while (resultSet.next()) {
                String fullName = resultSet.getString("full_name");
                String phoneNumber = resultSet.getString("phone_number");
                String emailAddress = resultSet.getString("email_address");
                Customer customer = new Customer(fullName, phoneNumber, emailAddress);
                allCustomers.add(customer);
            }
            logger.info("Selecting all customers from DB, retrieved {} customers", allCustomers.size());
        } catch (SQLException e) {
            logException(e);
        }
        return allCustomers;
    }

    public List<Employee> getAllEmployees() {
        List<Employee> allEmployees = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from employee");
            while (resultSet.next()) {
                String fullName = resultSet.getString("full_name");
                String phoneNumber = resultSet.getString("phone_number");
                String emailAddress = resultSet.getString("email_address");
                Employee employee = new Employee(fullName, phoneNumber, emailAddress, true);
                allEmployees.add(employee);
            }
            logger.info("Selecting all employees from DB, retrieved {} employees", allEmployees.size());
        } catch (SQLException e) {
            logException(e);
        }
        return allEmployees;
    }

    public Associate getAssociateByName(String fullName, boolean employee) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try(Connection connection = ConnectionUtil.getConnection()) {
            if (employee) { // look in employee table
                preparedStatement = connection.prepareStatement("select * from employee where full_name = ?");
            } else { // look in customer table
                preparedStatement = connection.prepareStatement("select * from customer where full_name = ?");
            }
            preparedStatement.setString(1, fullName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("full_name");
                String phoneNumber = resultSet.getString("phone_number");
                String emailAddress = resultSet.getString("email_address");
                if (employee) {
                    return new Associate(name, phoneNumber, emailAddress, true);
                } else {
                    return new Associate(name, phoneNumber, emailAddress);
                }
            }
        } catch (SQLException e) {
            logException(e);
        }
        // no record with the fullName
        logger.info("No associate with name {}", fullName);
        throw new NotFoundResponse("No such associate with name " + fullName);
    }

    public String getAssociateNameById(int id, boolean employee) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try (Connection connection = ConnectionUtil.getConnection()) {
            if (employee) {
                preparedStatement = connection.prepareStatement("select * from employee where id = ?");
            } else {
                preparedStatement = connection.prepareStatement("select * from customer where id = ?");
            }
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("full_name");
            } else {
                logger.info("Associate with id {} is not found", id);
            }
        } catch (SQLException e) {
            logException(e);
        }
        return null;
    }

    public int getAssociateIdByName(String name, boolean employee) {
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int id = 0;
        try (Connection connection = ConnectionUtil.getConnection()) {
            if (employee) {
                preparedStatement = connection.prepareStatement("select * from employee where full_name = ?");
            } else {
                preparedStatement = connection.prepareStatement("select * from customer where full_name = ?");
            }
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
            } else {
                logger.info("Associate {} is not found", name);
                throw new NotFoundResponse("Associate " + name + " not found");
            }
        } catch (SQLException e) {
            logException(e);
        }
        return id;
    }

    public void deleteEmployee(String fullName) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from employee where full_name = ?");
            preparedStatement.setString(1, fullName);
            preparedStatement.executeUpdate();
            logger.info("Deleted employee: {}", fullName);
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void updatePhoneNumber(String phoneNumber, String associateName, boolean employee) {
        PreparedStatement preparedStatement;
        try(Connection connection = ConnectionUtil.getConnection()) {
            if (employee) {
                preparedStatement = connection.prepareStatement("update employee set phone_number = ? where full_name = ?");
            } else {
                preparedStatement = connection.prepareStatement("update customer set phone_number = ? where full_name = ?");
            }
            preparedStatement.setString(1, phoneNumber);
            preparedStatement.setString(2, associateName);
            preparedStatement.executeUpdate();
            if (employee) {
                logger.info("Employee {}'s phone number updated", associateName);
            } else {
                logger.info("Customer {}'s phone number updated", associateName);
            }
        } catch (SQLException e) {
            logException(e);
        }
    }

    public void logException(Exception e){
        logger.error("{} - {}", e.getClass(), e.getMessage());
    }
}
