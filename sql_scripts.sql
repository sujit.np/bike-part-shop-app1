
create table part(
	id serial primary key,
	part_number Integer,
	part_name varchar(100),
	retail_price numeric(6, 2)
)

create table customer(
	id serial primary key,
	full_name varchar(50),
	phone_number varchar(14),
	email_address varchar(50)
)

 create table invoice(
	id serial primary key,
	invoice_number Integer,
	invoice_date Date,
	customer_id Integer references customer(id) on delete set null,
	employee_id Integer references employee(id) on delete set null
)
create table employee(
	id serial primary key,
	full_name varchar(50),
	phone_number varchar(14),
	email_address varchar(50)
)

create table part_invoice(
	invoice_id Integer references invoice(id) on delete set null,
	part_id Integer references part(id) on delete set null, 	
	quantity Integer
)

